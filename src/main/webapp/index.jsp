<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Displaying time</title>
</head>
<body>
    <jsp:useBean id="now" scope="page" class="java.util.Date"/>

    <fmt:setLocale value="fr_FR" scope="page"/>
    <fmt:formatDate value="${now}" pattern="dd MMMM yyyy HH:mm:ss" />
</body>
</html>
